from django.db import models
from datetime import timedelta, date
from django.core.validators import MaxValueValidator, MinValueValidator


SIX_MONTHS_FROM_NOW = date.today() + timedelta(180)


class Category(models.Model):
    name = models.CharField(max_length=125)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=125)
    description = models.TextField(blank=True)
    quantity = models.IntegerField(default=0)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    backorder_date = models.DateField(null=True, blank=True, default=SIX_MONTHS_FROM_NOW)
    discount_percent= models.IntegerField(
        default=0,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
        ]
     )
    category = models.ForeignKey(
        "Category", 
        on_delete=models.SET_NULL,
        related_name="products",
        null=True,
        )

    def __str__(self):
        return self.name


class Comment(models.Model):
    author = models.CharField(max_length=100)
    created = models.DateField(auto_now_add=True)
    content = models.TextField(max_length=1000)
    product = models.ForeignKey(
        "Product", related_name="comments", on_delete=models.CASCADE
    )

