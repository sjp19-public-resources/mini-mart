pip3 install django
echo What is the project name?
read project_name
django-admin startproject $project_name .
echo What is the first app name?
read app_name
python manage.py startapp $app_name
python manage.py migrate
python manage.py createsuperuser
